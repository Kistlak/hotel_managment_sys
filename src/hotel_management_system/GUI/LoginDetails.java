/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotel_management_system.GUI;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.sql.*;
/**
 *
 * @author user
 */
public class LoginDetails extends DBConnection{
    private String uname;
    private String pw;

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }
    
    public String login(){
        String s=null;
        String sql ="select * from login where username=? and pw=?";
        PreparedStatement pst = null;
        ResultSet rs = null;
        try{
            pst = getConnection().prepareStatement(sql);
            pst.setString(1, getUname());
            pst.setString(2, getPw());
            rs = pst.executeQuery();
            if(rs.next()){
                s="Login Success";
            }
            else{
                s="Login Error.. ";
            }
            
        }
        catch(Exception e){
            s = "error";
        }
        
        return s;
    }
    
}
